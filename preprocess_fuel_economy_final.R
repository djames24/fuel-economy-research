y2016<-read.csv("/Users/daniel/Desktop/Fuel Economy Research/SmartWay Vehicle List for MY 2016.csv",na.strings = c("N/A"));
y2015<-read.csv("/Users/daniel/Desktop/Fuel Economy Research/SmartWay Vehicle List for MY 2015.csv",na.strings = c("N/A"));
y2014<-read.csv("/Users/daniel/Desktop/Fuel Economy Research/SmartWay Vehicle List for MY 2014.csv",na.strings = c("N/A"));
y2013<-read.csv("/Users/daniel/Desktop/Fuel Economy Research/SmartWay Vehicle List for MY 2013.csv",na.strings = c("N/A"));
y2012<-read.csv("/Users/daniel/Desktop/Fuel Economy Research/SmartWay Vehicle List for MY 2012.csv",na.strings = c("N/A"));

colnames(y2016);
colnames(y2015);
colnames(y2014);
colnames(y2013);
colnames(y2012);

#Do some basic tests to confirm that the different variables (Smog.Rating, Air.Pollution.Score) measure the same thing.
mean(y2015$Smog.Rating);
sd(y2015$Smog.Rating);
mean(y2014$Air.Pollution.Score);
sd(y2014$Air.Pollution.Score);
var.test(y2015$Smog.Rating,y2014$Air.Pollution.Score,conf.level = .95);
hist(y2015$Smog.Rating);
hist(y2014$Air.Pollution.Score);

#The variables seem to be the same.

#2012-2014 used Air.Pollution.Score instead of Smog.Rating used in 2015-2016.
#2014 is the only year to include Comb.CO2 variable.
#We will remove the extra Comb.CO2 variable and standardize column names with those of most recent revision.
finalColumnNames<-colnames(y2016);
colnames(y2015)<-finalColumnNames;
y2014<-y2014[,1:ncol(y2016)];
colnames(y2014)<-finalColumnNames;
colnames(y2013)<-finalColumnNames;
colnames(y2012)<-finalColumnNames;

#Function that takes a data frame and splits the rows with cells containing combined data.
#It takes 3 params: data frame, column name, and separator string - returns a data frame.
splitRows<-function(data,var,sep){
  rows_num<-nrow(data);
  cols_num<-ncol(data);
  col_names<-colnames(data);
  split_col<-which(col_names == var);
  revisedDF<-NULL;
  for(i in 1:rows_num){
    splitString<-strsplit(as.character(data[i,var]),sep)[[1]];
    splitStringL<-length(splitString);
    if(splitStringL > 1){
      newRows<-matrix(data=NA,nrow=splitStringL,ncol=cols_num);
      for(j in 1:cols_num){
        splitComp<-strsplit(as.character(data[i,j]),sep)[[1]];
        for(k in 1:splitStringL) {
          if(length(splitComp) >= splitStringL) {
            newRows[k,j] = as.character(splitComp[k]);
          } else {
            newRows[k,j] = as.character(data[i,j]);
          }
        }
      }
      newRowsDF<-data.frame(newRows);
      colnames(newRowsDF) = col_names;
      revisedDF<-rbind(revisedDF,newRowsDF);
    } else {
      revisedDF<-rbind(revisedDF,data[i,]); 
    }
  }
  return(revisedDF);
}

#Split the rows with combined fuel columns.
splitFrame2016<-splitRows(y2016,"Fuel","/");
splitFrame2015<-splitRows(y2015,"Fuel","/");
splitFrame2014<-splitRows(y2014,"Fuel","/");
splitFrame2013<-splitRows(y2013,"Fuel","/");
splitFrame2012<-splitRows(y2012,"Fuel","/");

#Add in column for year.
splitFrame2016<-cbind(Year=rep(2016,nrow(splitFrame2016)),splitFrame2016);
splitFrame2015<-cbind(Year=rep(2015,nrow(splitFrame2015)),splitFrame2015);
splitFrame2014<-cbind(Year=rep(2014,nrow(splitFrame2014)),splitFrame2014);
splitFrame2013<-cbind(Year=rep(2013,nrow(splitFrame2013)),splitFrame2013);
splitFrame2012<-cbind(Year=rep(2012,nrow(splitFrame2012)),splitFrame2012);

#Merge the all of the years into a single dataset.
combinedData<-rbind(splitFrame2016,splitFrame2015,splitFrame2014,splitFrame2013,splitFrame2012);
  
write.csv(combinedData,"/Users/daniel/Desktop/Fuel Economy Research/combined.csv");




