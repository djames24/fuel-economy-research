# README
Version: 1.0

This file contains a basic description of the package contents.

### Files:
- **combined.csv**
  * contains the final dataset - years 2012-2016, split rows, added year column.
- **preprocess_fuel_economy_final.R**
  * contains the final script to preprocess the data - combines the datasets.
- **SmartWay Vehicle List for MY 2012.csv**
  * csv export of original dataset.
- **SmartWay Vehicle List for MY 2013.csv**
  * csv export of original dataset.
- **SmartWay Vehicle List for MY 2014.csv**
  * csv export of original dataset.
- **SmartWay Vehicle List for MY 2015.csv**
  * csv export of original dataset.
- **SmartWay Vehicle List for MY 2016.csv**
  * csv export of original dataset.
- **SmartWay Vehicle List for MY 2012.xlsx**
  * original dataset from fueleconomy.gov.
- **SmartWay Vehicle List for MY 2013.xlsx**
  * original dataset from fueleconomy.gov.
- **SmartWay Vehicle List for MY 2014.xlsx**
  * original dataset from fueleconomy.gov.
- **SmartWay Vehicle List for MY 2015.xlsx**
  * original dataset from fueleconomy.gov.
- **SmartWay Vehicle List for MY 2016.xlsx**
  * original dataset from fueleconomy.gov.

### Data Source:
All data can be found at https://www.fueleconomy.gov/feg/download.shtml
